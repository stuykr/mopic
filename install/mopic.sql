-- --------------------------------------------------------

--
-- Table structure for table `mp_tpl_main`
--

DROP TABLE IF EXISTS `mp_tpl_main`;
CREATE TABLE IF NOT EXISTS `mp_tpl_main` (
  `bn_no` int(11) NOT NULL AUTO_INCREMENT,
  `bn_state_ko` smallint(1) NOT NULL default '0',
  `bn_state_en` smallint(1) NOT NULL default '0',
  `bn_subject_ko` text NOT NULL,
  `bn_subject_en` text NOT NULL,
  `bn_content_ko` text NOT NULL,
  `bn_content_en` text NOT NULL,
  `bn_img_ko` varchar(100) NOT NULL default '',
  `bn_img_en` varchar(100) NOT NULL default '',
  `bn_link_ko` text NOT NULL,
  `bn_link_en` text NOT NULL,
  `bn_target` varchar(20) NOT NULL default '',
  `bn_sort_ko` int(10) NOT NULL default '0',
  `bn_sort_en` int(10) NOT NULL default '0',
  `bn_regdt` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`bn_no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mp_tpl_signage`
--

DROP TABLE IF EXISTS `mp_tpl_signage`;
CREATE TABLE IF NOT EXISTS `mp_tpl_signage` (
  `bn_no` int(11) NOT NULL AUTO_INCREMENT,
  `bn_state_ko` smallint(1) NOT NULL default '0',
  `bn_state_en` smallint(1) NOT NULL default '0',
  `bn_subject_ko` text NOT NULL,
  `bn_subject_en` text NOT NULL,
  `bn_content_ko` text NOT NULL,
  `bn_content_en` text NOT NULL,
  `bn_img_ko` varchar(100) NOT NULL default '',
  `bn_img_en` varchar(100) NOT NULL default '',
  `bn_link_ko` text NOT NULL,
  `bn_link_en` text NOT NULL,
  `bn_target` varchar(20) NOT NULL default '',
  `bn_sort_ko` int(10) NOT NULL default '0',
  `bn_sort_en` int(10) NOT NULL default '0',
  `bn_regdt` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`bn_no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `mp_tpl_monitor`
--

DROP TABLE IF EXISTS `mp_tpl_monitor`;
CREATE TABLE IF NOT EXISTS `mp_tpl_monitor` (
  `bn_no` int(11) NOT NULL AUTO_INCREMENT,
  `bn_state_ko` smallint(1) NOT NULL default '0',
  `bn_state_en` smallint(1) NOT NULL default '0',
  `bn_subject_ko` text NOT NULL,
  `bn_subject_en` text NOT NULL,
  `bn_content_ko` text NOT NULL,
  `bn_content_en` text NOT NULL,
  `bn_img_ko` varchar(100) NOT NULL default '',
  `bn_img_en` varchar(100) NOT NULL default '',
  `bn_link_ko` text NOT NULL,
  `bn_link_en` text NOT NULL,
  `bn_target` varchar(20) NOT NULL default '',
  `bn_sort_ko` int(10) NOT NULL default '0',
  `bn_sort_en` int(10) NOT NULL default '0',
  `bn_regdt` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`bn_no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mp_tpl_mobile`
--

DROP TABLE IF EXISTS `mp_tpl_mobile`;
CREATE TABLE IF NOT EXISTS `mp_tpl_mobile` (
  `bn_no` int(11) NOT NULL AUTO_INCREMENT,
  `bn_state_ko` smallint(1) NOT NULL default '0',
  `bn_state_en` smallint(1) NOT NULL default '0',
  `bn_subject_ko` text NOT NULL,
  `bn_subject_en` text NOT NULL,
  `bn_content_ko` text NOT NULL,
  `bn_content_en` text NOT NULL,
  `bn_img_ko` varchar(100) NOT NULL default '',
  `bn_img_en` varchar(100) NOT NULL default '',
  `bn_link_ko` text NOT NULL,
  `bn_link_en` text NOT NULL,
  `bn_target` varchar(20) NOT NULL default '',
  `bn_sort_ko` int(10) NOT NULL default '0',
  `bn_sort_en` int(10) NOT NULL default '0',
  `bn_regdt` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`bn_no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mp_tpl_business`
--

DROP TABLE IF EXISTS `mp_tpl_business`;
CREATE TABLE IF NOT EXISTS `mp_tpl_business` (
  `bn_no` int(11) NOT NULL AUTO_INCREMENT,
  `bn_state_ko` smallint(1) NOT NULL default '0',
  `bn_state_en` smallint(1) NOT NULL default '0',
  `bn_subject_ko` text NOT NULL,
  `bn_subject_en` text NOT NULL,
  `bn_content_ko` text NOT NULL,
  `bn_content_en` text NOT NULL,
  `bn_img_ko` varchar(100) NOT NULL default '',
  `bn_img_en` varchar(100) NOT NULL default '',
  `bn_link_ko` text NOT NULL,
  `bn_link_en` text NOT NULL,
  `bn_target` varchar(20) NOT NULL default '',
  `bn_sort_ko` int(10) NOT NULL default '0',
  `bn_sort_en` int(10) NOT NULL default '0',
  `bn_regdt` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`bn_no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mp_tpl_promotion`
--

DROP TABLE IF EXISTS `mp_tpl_promotion`;
CREATE TABLE IF NOT EXISTS `mp_tpl_promotion` (
  `bn_no` int(11) NOT NULL AUTO_INCREMENT,
  `bn_state_ko` smallint(1) NOT NULL default '0',
  `bn_state_en` smallint(1) NOT NULL default '0',
  `bn_subject_ko` text NOT NULL,
  `bn_subject_en` text NOT NULL,
  `bn_content_ko` text NOT NULL,
  `bn_content_en` text NOT NULL,
  `bn_img_ko` varchar(100) NOT NULL default '',
  `bn_img_en` varchar(100) NOT NULL default '',
  `bn_link_ko` text NOT NULL,
  `bn_link_en` text NOT NULL,
  `bn_target` varchar(20) NOT NULL default '',
  `bn_sort_ko` int(10) NOT NULL default '0',
  `bn_sort_en` int(10) NOT NULL default '0',
  `bn_regdt` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`bn_no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mp_tpl_business`
--

DROP TABLE IF EXISTS `mp_tpl_slider`;
CREATE TABLE IF NOT EXISTS `mp_tpl_slider` (
  `bn_no` int(11) NOT NULL AUTO_INCREMENT,
  `bn_state_ko` smallint(1) NOT NULL default '0',
  `bn_state_en` smallint(1) NOT NULL default '0',
  `bn_img_ko` varchar(100) NOT NULL default '',
  `bn_img_en` varchar(100) NOT NULL default '',
  `bn_link_ko` text NOT NULL,
  `bn_link_en` text NOT NULL,
  `bn_target` varchar(20) NOT NULL default '',
  `bn_sort_ko` int(10) NOT NULL default '0',
  `bn_sort_en` int(10) NOT NULL default '0',
  `bn_regdt` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`bn_no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
