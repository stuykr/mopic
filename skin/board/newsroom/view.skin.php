<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가
include_once(G5_LIB_PATH.'/thumbnail.lib.php');

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$board_skin_url.'/style.css">', 0);
?>

<script src="<?php echo G5_JS_URL; ?>/viewimageresize.js"></script>

<div class="bo-header">
	<div class="bo-title">
		<h2><?php echo $board['bo_subject'] ?></h2>
	</div>
</div>
<article id="board-view" style="width:<?php echo $width; ?>">
	<div class="bv-header">
		<h1 class="bv-title">
			<span class="bv-detail">
			<?php echo $view['wr_1']; ?>
			<?php if ($category_name) { echo '<span class="bv-category">'.$view['ca_name'].'</span>'; } ?>
			</span>
			<?php echo get_text($view['wr_subject']); ?>
		</h1>
	</div>
	<div class="bv-contents">
		<?php echo $view['content']; ?>
	</div>
	<div class="bo-footer">
		<?php if ($view['link']) { ?>
		<div class="bv-link">
			<ul>
			<?php
			// 링크
			$cnt = 0;
			for ($i=1; $i<=count($view['link']); $i++) {
				if ($view['link'][$i]) {
					$cnt++;
					$link = cut_str($view['link'][$i], 70);
			?>
				<li>
					<a href="<?php echo $view['link_href'][$i] ?>" target="_blank">
						<strong><?php echo $link ?></strong>
					</a>
				</li>
			<?php }} ?>
			</ul>
		</div>
		<?php } ?>
		<div class="btns">
			<div class="btns-common">
				<?php if ($prev_href || $next_href || $list_href) { ?>
				<ul>
					<?php if ($prev_href) { ?><li><a href="<?php echo $prev_href ?>" class="btn-prev"><i class="fa fa-angle-left" aria-hidden="true"></i> PREV</a></li><?php } ?>
					<?php if ($list_href) { ?><li><a href="<?php echo $list_href ?>" class="btn-list">LIST</a></li><?php } ?>
					<?php if ($next_href) { ?><li><a href="<?php echo $next_href ?>" class="btn-next">NEXT <i class="fa fa-angle-right" aria-hidden="true"></i></a></li><?php } ?>
				</ul>
				<?php } ?>
			</div>
			<div class="btns-default">
				<?php if ($update_href || $delete_href) { ?>
				<ul>
					<?php if ($update_href) { ?><li><a href="<?php echo $update_href ?>" class="btn-modify">MODIFY</a></li><?php } ?>
					<?php if ($delete_href) { ?><li><a href="<?php echo $delete_href ?>" class="btn-delete" onclick="del(this.href); return false;">DELETE</a></li><?php } ?>
					<?php if ($write_href) { ?><li><a href="<?php echo $write_href ?>" class="btn-write">WRITE</a></li><?php } ?>
				</ul>
				<?php } ?>
			</div>
		</div>
	</div>
</article>

<script>
<?php if ($board['bo_download_point'] < 0) { ?>
$(function() {
	$("a.view_file_download").click(function() {
		if(!g5_is_member) {
			alert("다운로드 권한이 없습니다.\n회원이시라면 로그인 후 이용해 보십시오.");
			return false;
		}

		var msg = "파일을 다운로드 하시면 포인트가 차감(<?php echo number_format($board['bo_download_point']) ?>점)됩니다.\n\n포인트는 게시물당 한번만 차감되며 다음에 다시 다운로드 하셔도 중복하여 차감하지 않습니다.\n\n그래도 다운로드 하시겠습니까?";

		if(confirm(msg)) {
			var href = $(this).attr("href")+"&js=on";
			$(this).attr("href", href);

			return true;
		} else {
			return false;
		}
	});
});
<?php } ?>

function board_move(href)
{
	window.open(href, "boardmove", "left=50, top=50, width=500, height=550, scrollbars=1");
}
</script>

<script>
$(function() {
	$('.bv-contents').viewimageresize();
});
</script>