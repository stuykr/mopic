<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_LIB_PATH.'/thumbnail.lib.php');

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$board_skin_url.'/style.css">', 0);
?>

<div class="bo-header">
<?php if (!$wr_id) { ?>
	<div class="bo-title">
		<h2><?php echo $board['bo_subject'] ?><span class="sound_only"> 목록</span></h2>
	</div>
	<?php if ($is_category) { ?>
	<nav class="bo-category">
		<ul>
			<?php echo $category_option ?>
		</ul>
	</nav>
	<?php } ?>
<?php } ?>
	<div class="bo-search">
		<fieldset id="bo_sch">
			<legend class="blind">게시물 검색</legend>
			<form name="fsearch" method="get">
			<input type="hidden" name="bo_table" value="<?php echo $bo_table ?>">
			<input type="hidden" name="sca" value="<?php echo $sca ?>">
			<input type="hidden" name="sop" value="and">
			<input type="hidden" name="sfl" id="sfl" value="wr_subject||wr_content"<?php echo get_selected($sfl, 'wr_subject||wr_content'); ?>>
			<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
			<input type="text" name="stx" value="<?php echo stripslashes($stx) ?>" required id="stx" class="btn-input" maxlength="30" placeholder="Search Keyword...">
			<button type="submit" class="btn-search"><i class="fa fa-search" aria-hidden="true"></i></button>
			</form>
		</fieldset>
	</div>
</div>

<form name="fboardlist"  id="fboardlist" action="./board_list_update.php" onsubmit="return fboardlist_submit(this);" method="post">
<input type="hidden" name="bo_table" value="<?php echo $bo_table ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="spt" value="<?php echo $spt ?>">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="sw" value="">
<div class="bo-contents">
	<ul class="list-thumb">
		<?php for ($i=0; $i<count($list); $i++) { ?>
		<?php if ($wr_id == $list[$i]['wr_id']) { ?>
		<li class="gall_now">
		<?php } else { ?>
		<li>
		<?php } ?>
			<span class="sound_only">
				<?php
				if ($wr_id == $list[$i]['wr_id'])
					echo "<span class=\"bo_current\">열람중</span>";
				else
					echo $list[$i]['num'];
				?>
			</span>
			<a href="<?php echo $list[$i]['href'] ?>">
				<?php
					$thumb = get_list_thumbnail($board['bo_table'], $list[$i]['wr_id'], $board['bo_gallery_width'], $board['bo_gallery_height']);
					if ($thumb['src']) {
						$img_content = '<img src="'.$thumb['src'].'" alt="'.$thumb['alt'].'">';
					} else {
						$img_content = '<img src="'.$board_skin_url.'/no_img.jpg">';
					}
				?>
				<p class="thumb"><?php echo $img_content; ?></p>
				<div class="info">
					<p class="date"><span><?php echo $list[$i]['wr_1'] ?></span><?php if ($is_category && $list[$i]['ca_name']) { ?><span class="cate"><?php echo $list[$i]['ca_name'] ?></span><?php } ?></p>
					<p class="title">
					<?php if ($is_checkbox) { ?>
					<label for="chk_wr_id_<?php echo $i ?>" class="sound_only"><?php echo $list[$i]['subject'] ?></label>
					<input type="checkbox" name="chk_wr_id[]" value="<?php echo $list[$i]['wr_id'] ?>" id="chk_wr_id_<?php echo $i ?>">
					<?php } ?>
					<?php echo $list[$i]['subject'] ?></p>
					<?php if ($list[$i]['content']) { ?>
					<p class="desc"><?php echo cut_str(strip_tags($list[$i]['content']), 135) ?></p>
					<?php } ?>
				</div>
			</a>
		</li>
		<?php } ?>
		<?php if (count($list) == 0) { echo "<li class=\"empty_list\">No data to display</li>"; } ?>
	</ul>
</div>

<div class="bo-footer">
	<div class="bo-paging">
		<?php echo $write_pages;  ?>
	</div>
	<div class="btns">
		<div class="btns-admin">
			<?php if ($is_checkbox) { ?>
			<ul>
				<li><input type="submit" name="btn_submit" value="DELETE" onclick="document.pressed=this.value" class="btn-chk-submit"></li>
				<li><input type="submit" name="btn_submit" value="DUPLICATE" onclick="document.pressed=this.value" class="btn-chk-submit"></li>
				<li><input type="submit" name="btn_submit" value="MOVE" onclick="document.pressed=this.value" class="btn-chk-submit"></li>
				<?php if ($admin_href) { ?><li><a href="<?php echo $admin_href ?>" class="btn-admin">ADMIN</a></li><?php } ?>
			</ul>
			<?php } ?>
		</div>
		<div class="btns-default">
			<?php if ($list_href || $write_href) { ?>
			<ul>
				<?php if ($list_href) { ?><li><a href="<?php echo $list_href ?>" class="btn-list">LIST</a></li><?php } ?>
				<?php if ($write_href) { ?><li><a href="<?php echo $write_href ?>" class="btn-write">WRITE</a></li><?php } ?>
			</ul>
			<?php } ?>
		</div>
	</div>
</div>
</form>

<?php if ($is_checkbox) { ?>
<noscript>
<p>자바스크립트를 사용하지 않는 경우<br>별도의 확인 절차 없이 바로 선택삭제 처리하므로 주의하시기 바랍니다.</p>
</noscript>
<?php } ?>
<?php if ($is_checkbox) { ?>
<script>
function all_checked(sw) {
	var f = document.fboardlist;

	for (var i=0; i<f.length; i++) {
		if (f.elements[i].name == "chk_wr_id[]")
			f.elements[i].checked = sw;
	}
}

function fboardlist_submit(f) {
	var chk_count = 0;

	for (var i=0; i<f.length; i++) {
		if (f.elements[i].name == "chk_wr_id[]" && f.elements[i].checked)
			chk_count++;
	}

	if (!chk_count) {
		alert(document.pressed + "할 게시물을 하나 이상 선택하세요.");
		return false;
	}

	if(document.pressed == "DUPLICATE") {
		select_copy("copy");
		return;
	}

	if(document.pressed == "MOVE") {
		select_copy("move");
		return;
	}

	if(document.pressed == "DELETE") {
		if (!confirm("선택한 게시물을 정말 삭제하시겠습니까?\n\n한번 삭제한 자료는 복구할 수 없습니다\n\n답변글이 있는 게시글을 선택하신 경우\n답변글도 선택하셔야 게시글이 삭제됩니다."))
			return false;

		f.removeAttribute("target");
		f.action = "./board_list_update.php";
	}

	return true;
}

// 선택한 게시물 복사 및 이동
function select_copy(sw) {
	var f = document.fboardlist;

	if (sw == 'copy')
		str = "복사";
	else
		str = "이동";

	var sub_win = window.open("", "move", "left=50, top=50, width=500, height=550, scrollbars=1");

	f.sw.value = sw;
	f.target = "move";
	f.action = "./move.php";
	f.submit();
}
</script>
<?php } ?>
<!-- } 게시판 목록 끝 -->
