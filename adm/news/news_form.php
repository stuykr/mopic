<?php
$sub_menu = '700010';
include_once('./_common.php');
include_once(G5_EDITOR_LIB);

include_once(G5_LIB_PATH.'/thumbnail.lib.php');

auth_check($auth[$sub_menu], "w");

$html_title = "Newsroom";
 

if ($w == "u")
{
    $html_title .= " 수정";
    $sql = " select * from MP_NEWS where nw_id = '$nw_id' ";
    $row = sql_fetch($sql);
    if (!$row['nw_id']) alert("등록된 자료가 없습니다.");
}
else
{
    $html_title .= " 입력"; 
}

$g5['title'] = $html_title;
include_once (G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');
?>

<form name="frmnews_" action="./news_formupdate.php" onsubmit="return frmnews__check(this);" method="post" enctype="multipart/form-data">
<input type="hidden" name="w" value="<?php echo $w; ?>">
<input type="hidden" name="nw_id" value="<?php echo $nw_id; ?>">
<input type="hidden" name="token" value="">
 

<div class="tbl_frm01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?></caption>
    <colgroup>
        <col class="grid_4">
        <col>
    </colgroup>
    <tbody>
    <tr>
        <th scope="row">국가분류</th>
        <td>
            <select name="nw_cate" class="frm_input" required>
			<option value="">분류선택</option>
			<?
			for ($i=1; $i<count($nation_news_arr); $i++) {
				$str_sel = "";
				if($row['nw_cate'] && $i==$row['nw_cate']) $str_sel="selected";
			?>
			<option value="<?=$i?>" <?=$str_sel?>><?=$nation_news_arr[$i]?></option>
			<?}?>
			</select>
        </td>
    </tr>
	<tr>
        <th scope="row">제목</th>
        <td>
            <input type="text" name="nw_subject" value="<?php echo $row['nw_subject'] ?>" class="frm_input" style="width:90%;">
        </td>
    </tr>
	<tr>
        <th scope="row">날짜</th>
        <td>
            <input type="text" name="nw_day" value="<?php echo $row['nw_day'] ?>" class="frm_input daypicker_input" size="12" readonly>
        </td>
    </tr>
	<tr>
        <th scope="row">내용</th>
        <td>
            <?php echo editor_html('nw_contents', get_text($row['nw_contents'], 0)); ?>
        </td>
    </tr>

    </tbody>
    </table>
</div>

<div class="btn_confirm01 btn_confirm">
    <input type="submit" value="확인" class="btn_submit" accesskey="s">
    <a href="./news_list.php">목록</a>
</div>
</form>

<script>

$(function(){
	$(".daypicker_input").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99" });
});

function frmnews__check(f)
{
    errmsg = "";
    errfld = ""; 

    //check_field(f.nw_subject, "제목을 입력하세요.");

    if (errmsg != "") {
        alert(errmsg);
        errfld.focus();
        return false;
    }

	<?php echo get_editor_js('nw_contents'); ?>
    return true;
}
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>
