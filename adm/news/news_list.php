<?php
$sub_menu = '700010';
include_once('./_common.php');

include_once(G5_LIB_PATH.'/thumbnail.lib.php');

auth_check($auth[$sub_menu], "r");
 

$g5['title'] = 'Newsroom 관리';
include_once (G5_ADMIN_PATH.'/admin.head.php');

$sql_common = " from MP_NEWS ";

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$sql = "select * $sql_common order by nw_id desc ";
$result = sql_query($sql);
?>

<div class="local_ov01 local_ov">전체 <?php echo $total_count; ?>건</div>

<div class="btn_add01 btn_add">
    <a href="./news_form.php">Newsroom등록</a>
</div>

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">분류</th>
        <th scope="col">제목</th>
        <th scope="col">날짜</th>
		<th scope="col">등록일</th>
        <th scope="col">관리</th>
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {
        $bg = 'bg'.($i%2); 

		//$thumb = get_app_thumbnail("MP_NEWS", $row['nw_id'], 60, 60);
		//$pt_img = $thumb['src']; 
    ?>
    <tr class="<?php echo $bg; ?>">
        <td class="td_tel" align="center"><?php echo $nation_news_arr[$row['nw_cate']]; ?></td>
        <td align="left">&nbsp;&nbsp;&nbsp;<a href="./news_form.php?w=u&amp;nw_id=<?php echo $row['nw_id']; ?>"><?php echo $row['nw_subject']; ?></a></td>
		<td class="td_datetime"><?php echo $row['nw_day']; ?></td>
        <td class="td_datetime"><?php echo $row['nw_regdate']; ?></td>
        <td class="td_mngsmall">
            <a href="./news_form.php?w=u&amp;nw_id=<?php echo $row['nw_id']; ?>"><span class="sound_only"><?php echo $row['nw_subject']; ?> </span>수정</a>
            <a href="./news_formupdate.php?w=d&amp;nw_id=<?php echo $row['nw_id']; ?>" onclick="return delete_confirm('<?php echo $row['nw_id']; ?>');"><span class="sound_only"><?php echo $row['nw_subject']; ?> </span>삭제</a>
        </td>
    </tr>
    <?php
    }

    if ($i == 0) {
        echo '<tr><td colspan="11" class="empty_table">자료가 한건도 없습니다.</td></tr>';
    }
    ?>
    </tbody>
    </table>
</div>

<script type="text/javascript">
<!--

function delete_confirm(nw_id){
	if(confirm("한번 삭제한 자료는 복구할 방법이 없습니다.\n\n정말 삭제하시겠습니까?")) {
		var url = "./news_formupdate.php?w=d&nw_id="+nw_id;
		location.href=url;    
	}

}

//-->
</script>


<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>
