<?php
$sub_menu = '600010';
include_once('./_common.php');
include_once(G5_EDITOR_LIB);

include_once(G5_LIB_PATH.'/thumbnail.lib.php');

auth_check($auth[$sub_menu], "w");

$html_title = "상품";
 

if ($w == "u")
{
    $html_title .= " 수정";
    $sql = " select * from MP_ITEM where it_id = '$it_id' ";
    $row = sql_fetch($sql);
    if (!$row['it_id']) alert("등록된 자료가 없습니다.");
}
else
{
    $html_title .= " 입력"; 
}

$g5['title'] = $html_title;
include_once (G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');
?>

<form name="frmitem_" action="./item_formupdate.php" onsubmit="return frmitem__check(this);" method="post" enctype="multipart/form-data">
<input type="hidden" name="w" value="<?php echo $w; ?>">
<input type="hidden" name="it_id" value="<?php echo $it_id; ?>">
<input type="hidden" name="token" value="">
 

<div class="tbl_frm01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?></caption>
    <colgroup>
        <col class="grid_4">
        <col>
    </colgroup>
    <tbody>
    <tr>
        <th scope="row">상품분류</th>
        <td>
            <select name="it_cate" class="frm_input" required>
			<option value="">분류선택</option>
			<?
			for ($i=1; $i<count($item_cate_arr); $i++) {
				$str_sel = "";
				if($row['it_cate'] && $i==$row['it_cate']) $str_sel="selected";
			?>
			<option value="<?=$i?>" <?=$str_sel?>><?=$item_cate_arr[$i]?></option>
			<?}?>
			</select>
        </td>
    </tr>
	<tr>
        <th scope="row">상품명</th>
        <td>
            <input type="text" name="it_name" value="<?php echo $row['it_name'] ?>" class="frm_input" size="60">
        </td>
    </tr>
	<tr>
        <th scope="row">모델명</th>
        <td>
            <input type="text" name="it_model" value="<?php echo $row['it_model'] ?>" class="frm_input" size="60">
        </td>
    </tr>
    <tr>
        <th scope="row">상품썸네일</th>
        <td>
			<?
			$i = 0;
			?>
			<input type="file" name="bf_file[<?=$i?>]" id="file">
			<?php
			$thumb = get_app_thumbnail("MP_ITEM", $row['it_id'], 60, 60);
			$pt_img = $thumb['src']; 
			if($pt_img){
			?>
			<div>
			<a href="<?=$thumb['ori']?>" target="_blank"><img class="img-responsive" src="<?=$pt_img?>" alt=""></a>
			<br>
			<input type="checkbox" id="bf_file_del<?=$$i?>" name="bf_file_del[<?=$i?>]" value="1"> 
			<label for="bf_file_del<?=$i?>"> 파일 삭제</label>
			</div>
			<?php } ?>
        </td>
    </tr>
	
	<?
	for ($i=1; $i<5; $i++) {
	?>
	 <tr>
        <th scope="row">상품설명-<?=$nation_arr[$i]?></th>
        <td>
			<input type="file" name="bf_file[<?=$i?>]" id="file">
			<?php
			$thumb = get_app_thumbnail("MP_ITEM", $row['it_id'], 600, 600, $i);
			$pt_img = $thumb['src']; 
			if($pt_img){
			?>
			<div>
			<a href="<?=$thumb['ori']?>" target="_blank"><img class="img-responsive" src="<?=$pt_img?>" alt=""></a>
			<br>
			<input type="checkbox" id="bf_file_del<?=$$i?>" name="bf_file_del[<?=$i?>]" value="1"> 
			<label for="bf_file_del<?=$i?>"> 파일 삭제</label>
			</div>
			<?php } ?>
        </td>
    </tr>
	<?php } ?>

    <tr>
        <th scope="row">구매하기링크</th>
        <td>
            <input type="text" name="it_url" value="<?php echo $row['it_url'] ?>" class="frm_input" style="width:90%;">
        </td>
    </tr>
    </tbody>
    </table>
</div>

<div class="btn_confirm01 btn_confirm">
    <input type="submit" value="확인" class="btn_submit" accesskey="s">
    <a href="./item_list.php">목록</a>
</div>
</form>

<script>

$(function(){
	$(".daypicker_input").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99" });
});

function frmitem__check(f)
{
    errmsg = "";
    errfld = ""; 

    //check_field(f.nw_subject, "제목을 입력하세요.");

    if (errmsg != "") {
        alert(errmsg);
        errfld.focus();
        return false;
    }

	<?php //echo get_editor_js('it_explan'); ?>
    return true;
}
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>
