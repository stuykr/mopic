<?php
$sub_menu = '600010';
include_once('./_common.php');

include_once(G5_LIB_PATH.'/thumbnail.lib.php');

auth_check($auth[$sub_menu], "r");
 

$g5['title'] = '상품관리(사용X)';
include_once (G5_ADMIN_PATH.'/admin.head.php');

$sql_common = " from MP_ITEM ";

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$sql = "select * $sql_common order by it_id desc ";
$result = sql_query($sql);
?>

<div class="local_ov01 local_ov">전체 <?php echo $total_count; ?>건</div>

<div class="btn_add01 btn_add">
    <a href="./item_form.php">상품등록</a>
</div>

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">상품분류</th>
        <th scope="col">상품썸네일</th>
        <th scope="col">상품명</th>
        <th scope="col">모델명</th>
        <th scope="col">구매하기링크</th>
		<th scope="col">등록일</th>
        <th scope="col">관리</th>
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {
        $bg = 'bg'.($i%2); 

		$thumb = get_app_thumbnail("MP_ITEM", $row['it_id'], 60, 60);
		$pt_img = $thumb['src']; 
    ?>
    <tr class="<?php echo $bg; ?>">
        <td class="td_tel" align="center"><?php echo $item_cate_arr[$row['it_cate']]; ?></td>
        <td class="center" style="padding:10px; width:60px;">
																	<?php
																	if($pt_img){
																	?>
																	<img class="img-responsive" src="<?=$pt_img?>" alt="">
																	<?php
																	}else{
																	?>
																	<?php }?>
		</td>
        <td align="center"><a href="./item_form.php?w=u&amp;it_id=<?php echo $row['it_id']; ?>"><?php echo $row['it_name']; ?></a></td>
		<td align="center"><?php echo $row['it_model']; ?></td>
		<td align="center"><?php if($row['it_url']){?><a href="<?php echo $row['it_url']; ?>">LINK</a><?php } ?></td>
        <td class="td_datetime"><?php echo $row['it_regdate']; ?></td>
        <td class="td_mngsmall">
            <a href="./item_form.php?w=u&amp;it_id=<?php echo $row['it_id']; ?>"><span class="sound_only"><?php echo $row['nw_subject']; ?> </span>수정</a>
            <a href="./item_formupdate.php?w=d&amp;it_id=<?php echo $row['it_id']; ?>" onclick="return delete_confirm('<?php echo $row['it_id']; ?>');"><span class="sound_only"><?php echo $row['nw_subject']; ?> </span>삭제</a>
        </td>
    </tr>
    <?php
    }

    if ($i == 0) {
        echo '<tr><td colspan="11" class="empty_table">자료가 한건도 없습니다.</td></tr>';
    }
    ?>
    </tbody>
    </table>
</div>

<script type="text/javascript">
<!--

function delete_confirm(it_id){
	if(confirm("한번 삭제한 자료는 복구할 방법이 없습니다.\n\n정말 삭제하시겠습니까?")) {
		var url = "./item_formupdate.php?w=d&it_id="+it_id;
		location.href=url;    
	}

}

//-->
</script>


<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>
