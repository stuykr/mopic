<?php
$sub_menu = "700010";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'w');

if (!isset($type) || $type == '') {
	alert('잘못된 접근입니다!');
}

$bn_sort_ko = (isset($_POST['bn_sort_ko']) && $_POST['bn_sort_ko'] != '') ? $_POST['bn_sort_ko'] : 0;
$bn_sort_en = (isset($_POST['bn_sort_en']) && $_POST['bn_sort_en'] != '') ? $_POST['bn_sort_en'] : 0;
$is_bn_img_ko = false;
$is_bn_img_en = false;

$sql_common = "  bn_sort_ko = '{$bn_sort_ko}',
				 bn_sort_en = '{$bn_sort_en}',
				 bn_state_ko = '{$_POST['bn_state_ko']}',
				 bn_state_en = '{$_POST['bn_state_en']}',
				 bn_subject_ko = '".addslashes($_POST['bn_subject_ko'])."',
				 bn_subject_en = '".addslashes($_POST['bn_subject_en'])."',
				 bn_content_ko = '".addslashes($_POST['bn_content_ko'])."',
				 bn_content_en = '".addslashes($_POST['bn_content_en'])."',
				 bn_link_ko = '{$_POST['bn_link_ko']}',
				 bn_link_en = '{$_POST['bn_link_en']}',
				 bn_target = '{$_POST['bn_target']}',
";

// 배너 이미지 업로드
// 외부 이미지 경로가 있을 경우 자체 업로드보다 높은 우선순위 적용
if ($_POST['bn_img_ko_external']) {
	if (!preg_match("/\.(jpg|gif|png)$/i", $_POST['bn_img_ko_external'])) {
		alert($_POST['bn_img_ko_external'] . '은(는) jpg/gif/png 파일이 아닙니다.');
	} else if (!preg_match("/(http|https):/i", $_POST['bn_img_ko_external'])) {
		alert($_POST['bn_img_ko_external'] . '는 올바른 이미지 경로가 아닙니다.');
	} else {
		$file_name_ko = $_POST['bn_img_ko_external'];
		$sql_common .= "bn_img_ko = '". $file_name_ko ."',";
	}
} else {
	if (is_uploaded_file($_FILES['bn_img_ko']['tmp_name'])) {
		$ext = $qfile->get_file_ext($_FILES['bn_img_ko']['name']);
		$file_name_ko = md5(time().$_FILES['bn_img_ko']['name']).".".$ext;
		if (!preg_match("/\.(jpg|gif|png)$/i", $_FILES['bn_img_ko']['name'])) {
			alert($_FILES['bn_img_ko']['name'] . '은(는) jpg/gif/png 파일이 아닙니다.');
		}
	
		if (preg_match("/\.(jpg|gif|png)$/i", $_FILES['bn_img_ko']['name'])) {
			@mkdir(G5_DATA_PATH.'/tpl_'.$type.'/', G5_DIR_PERMISSION);
			@chmod(G5_DATA_PATH.'/tpl_'.$type.'/', G5_DIR_PERMISSION);
	
			$dest_path = G5_DATA_PATH.'/tpl_'.$type.'/'.$file_name_ko;
	
			move_uploaded_file($_FILES['bn_img_ko']['tmp_name'], $dest_path);
			chmod($dest_path, G5_FILE_PERMISSION);
	
			if (file_exists($dest_path)) {
				$size = getimagesize($dest_path);
				$sql_common .= "bn_img_ko = '". $file_name_ko ."',";
				$is_bn_img_ko = true;
			}
		}
	}
}

if ($_POST['bn_img_en_external']) {
	if (!preg_match("/\.(jpg|gif|png)$/i", $_POST['bn_img_en_external'])) {
		alert($_POST['bn_img_en_external'] . '은(는) jpg/gif/png 파일이 아닙니다.');
	} else if (!preg_match("/(http|https):/i", $_POST['bn_img_en_external'])) {
		alert($_POST['bn_img_en_external'] . '는 올바른 이미지 경로가 아닙니다.');
	} else {
		$file_name_en = $_POST['bn_img_en_external'];
		$sql_common .= "bn_img_en = '". $file_name_en ."',";
	}
} else {
	if (is_uploaded_file($_FILES['bn_img_en']['tmp_name'])) {
		$ext = $qfile->get_file_ext($_FILES['bn_img_en']['name']);
		$file_name_en = md5(time().$_FILES['bn_img_en']['name']).".".$ext;
		if (!preg_match("/\.(jpg|gif|png)$/i", $_FILES['bn_img_en']['name'])) {
			alert($_FILES['bn_img_en']['name'] . '은(는) jpg/gif/png 파일이 아닙니다.');
		}
	
		if (preg_match("/\.(jpg|gif|png)$/i", $_FILES['bn_img_en']['name'])) {
			@mkdir(G5_DATA_PATH.'/tpl_'.$type.'/', G5_DIR_PERMISSION);
			@chmod(G5_DATA_PATH.'/tpl_'.$type.'/', G5_DIR_PERMISSION);
	
			$dest_path = G5_DATA_PATH.'/tpl_'.$type.'/'.$file_name_en;
	
			move_uploaded_file($_FILES['bn_img_en']['tmp_name'], $dest_path);
			chmod($dest_path, G5_FILE_PERMISSION);
	
			if (file_exists($dest_path)) {
				$size = getimagesize($dest_path);
				$sql_common .= "bn_img_en = '". $file_name_en ."',";
				$is_bn_img_en = true;
			}
		}
	}
}

if ($w == '') {
	sql_query(" insert into mp_tpl_{$type} set {$sql_common} bn_regdt = '".G5_TIME_YMDHIS."'");
	$bn_no = sql_insert_id();
	$msg = $type." 화면 섹션을 추가하였습니다.";
} else if ($w == 'u') {
	if ($_POST['del_bn_url_ko'] == '1') {
		$banner_file_ko = G5_DATA_PATH.'/tpl_'.$type.'/'.$_POST['del_bn_url_ko_name'];
		if (file_exists($banner_file_ko)) {
			@unlink($banner_file_ko);
		}
		if (!$is_bn_img_ko) {
			$sql_common .= "bn_img_ko = '',";
		}
	}
	if ($_POST['del_bn_url_en'] == '1') {
		$banner_file_en = G5_DATA_PATH.'/tpl_'.$type.'/'.$_POST['del_bn_url_en_name'];
		if (file_exists($banner_file_en)) {
			@unlink($banner_file_en);
		}
		if (!$is_bn_img_en) {
			$sql_common .= "bn_img_en = '',";
		}
	}
	$sql = " update mp_tpl_{$type} set {$sql_common} bn_regdt=bn_regdt where bn_no = '{$bn_no}' ";
	sql_query($sql);
	$msg = $type. " 화면 섹션을 정상적으로 수정하였습니다.";
} else {
	alert('제대로 된 값이 넘어오지 않았습니다.');
}

alert($msg,'./tpl_section_form.php?'.$qstr.'&amp;type='.$type.'&amp;w=u&amp;bn_no='.$bn_no);
?>