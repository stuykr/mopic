<?php
$sub_menu = "700010";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'w');

if (!isset($type) || $type == '') {
	alert('잘못된 접근입니다!');
}

if ($w == '')
{
	$sound_only = '<strong class="sound_only">필수</strong>';
	$html_title = '추가';
}
else if ($w == 'u')
{
	$row = sql_fetch("select * from mp_tpl_{$type} where bn_no = '{$bn_no}'");
	foreach($row as $k => $v) {
		$banner[$k] = $v;
	}
	if (!$banner['bn_no'])
		alert('존재하지 않는 섹션입니다.');

	$html_title = '수정';

	$banner['bn_sort_ko'] = get_text($banner['bn_sort_ko']);
	$banner['bn_sort_en'] = get_text($banner['bn_sort_en']);
	$banner['bn_target'] = get_text($banner['bn_target']);
	$banner['bn_img_ko'] = get_text($banner['bn_img_ko']);
	$banner['bn_img_en'] = get_text($banner['bn_img_en']);
	$banner['bn_state_ko'] = get_text($banner['bn_state_ko']);
	$banner['bn_state_en'] = get_text($banner['bn_state_en']);
	$banner['bn_link_ko'] = get_text($banner['bn_link_ko']);
	$banner['bn_link_en'] = get_text($banner['bn_link_en']);
	$banner['bn_subject_ko'] = stripslashes($banner['bn_subject_ko']);
	$banner['bn_subject_en'] = stripslashes($banner['bn_subject_en']);
	$banner['bn_content_ko'] = stripslashes($banner['bn_content_ko']);
	$banner['bn_content_en'] = stripslashes($banner['bn_content_en']);
}
else
	alert('제대로 된 값이 넘어오지 않았습니다.');

$g5['title'] = '디자인 섹션 관리 - '.strtoupper($type).' '.$html_title;
include_once(G5_ADMIN_PATH.'/admin.head.php');

// add_javascript('js 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_javascript(G5_POSTCODE_JS, 0);    //다음 주소 js
?>
<form name="fbanner" id="fbanner" action="./tpl_section_form_update.php?type=<?php echo $type; ?>" onsubmit="return fbanner_submit(this);" method="post" enctype="multipart/form-data">
<input type="hidden" name="w" value="<?php echo $w ?>">
<input type="hidden" name="bn_no" value="<?php echo $bn_no ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">

<h2 class="h2_frm" style="color:#ff3061">[한/영] 공통 설정</h2>
<div class="tbl_frm01 tbl_wrap">
	<table>
		<colgroup>
			<col class="grid_4">
			<col>
			<col class="grid_4">
			<col>
		</colgroup>
		<tbody>
			<tr>
				<th scope="row"><label for="bn_target">[한/영 공통] 링크 새창 여부</label></th>
				<td colspan="4">
					<select name="bn_target" id="bn_target" required class="frm_input">
						<option value="">타겟을 선택하세요.</option>
						<option value="_blank" <?php if ($banner['bn_target']=='_blank') echo "selected";?>>새창</option>
						<option value="_self" <?php if ($banner['bn_target'] == '_self' || $banner['bn_target'] != '_blank') echo "selected";?>>현재창</option>
					</select>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<h2 class="h2_frm" style="color:#ff3061">[한국어] 섹션 설정</h2>
<div class="tbl_frm01 tbl_wrap">
	<table>
		<caption><?php echo $g5['title']; ?></caption>
		<colgroup>
			<col class="grid_4">
			<col>
			<col class="grid_4">
			<col>
		</colgroup>
		<tbody>
			<tr>
				<th scope="row"><label for="bn_state_ko">게시여부</label></th>
				<td>
					<select name="bn_state_ko" id="bn_state_ko" required class="frm_input">
						<option value="">선택</option>
						<option value="1" <?php if ($banner['bn_state_ko'] != '2') echo "selected";?>>섹션 보이기</option>
						<option value="2" <?php if ($banner['bn_state_ko'] == '2') echo "selected";?>>섹션 숨기기</option>
					</select>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="bn_sort_ko">정렬 순번</label></th>
				<td>
					<input type="text" name="bn_sort_ko" id="bn_sort_ko" class="frm_input" size="10" value="<?php echo $banner['bn_sort_ko']?>"> 숫자가 높을 수록 위에 노출됩니다.
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="bn_img_ko">섹션 이미지(자체 업로드)</label></th>
				<td colspan="3">
					<input type="file" name="bn_img_ko" id="bn_img_ko">
					<?php
					$bn_file_ko = G5_DATA_PATH.'/tpl_'.$type.'/'.$banner['bn_img_ko'];
					if (file_exists($bn_file_ko) && $banner['bn_img_ko']) {
						$bn_url_ko = G5_DATA_URL.'/tpl_'.$type.'/'.$banner['bn_img_ko'];
						echo '<img src="'.$bn_url_ko.'" alt="" height=80> ';
						echo '<input type="checkbox" id="del_bn_url_ko" name="del_bn_url_ko" value="0"><input type="hidden" name="del_bn_url_ko_name" id="del_bn_url_ko_name" value="'.$banner['bn_img_ko'].'">삭제';
					} else {
						echo '<input type="hidden" name="del_bn_url_ko_name" id="del_bn_url_ko_name" value="">';
					}
					?>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="bn_img_ko_external">섹션 이미지(외부 이미지)</label></th>
				<td colspan="3">
					<?php
						if ($banner['bn_img_ko'] && preg_match("/(http|https):/i", $banner['bn_img_ko'])) {
							echo '<img src="'.$banner['bn_img_ko'].'" alt="" height=80><br><br>';
							echo '<input type="text" name="bn_img_ko_external" id="bn_img_ko_external" class="frm_input" size="80" value="'.$banner['bn_img_ko'].'">';
						} else {
							echo '<input type="text" name="bn_img_ko_external" id="bn_img_ko_external" class="frm_input" size="80" value="">';
						}
					?>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="bn_link_ko">섹션 링크</label></th>
				<td>
					<input type="text" name="bn_link_ko" id="bn_link_ko" class="frm_input" size="80" value="<?php echo $banner['bn_link_ko']?>">
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="bn_subject_ko">섹션 제목</label></th>
				<td>
					<input type="text" name="bn_subject_ko" id="bn_subject_ko" class="frm_input" size="80" value='<?php echo $banner['bn_subject_ko']?>'>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="bn_content_ko">섹션 내용</label></th>
				<td colspan="3"><textarea name="bn_content_ko" id="bn_content_ko" style="height:100px;"><?php echo $banner['bn_content_ko'] ?></textarea></td>
			</tr>
		</tbody>
	</table>
</div>
<h2 class="h2_frm" style="color:#ff3061">[영어] 섹션 설정</h2>
<div class="tbl_frm01 tbl_wrap">
	<table>
		<caption><?php echo $g5['title']; ?></caption>
		<colgroup>
			<col class="grid_4">
			<col>
			<col class="grid_4">
			<col>
		</colgroup>
		<tbody>
			<tr>
				<th scope="row"><label for="bn_state_en">게시여부</label></th>
				<td>
					<select name="bn_state_en" id="bn_state_en" required class="frm_input">
						<option value="">선택</option>
						<option value="1" <?php if ($banner['bn_state_en'] != '2') echo "selected";?>>섹션 보이기</option>
						<option value="2" <?php if ($banner['bn_state_en'] == '2') echo "selected";?>>섹션 숨기기</option>
					</select>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="bn_sort_en">정렬 순번</label></th>
				<td>
					<input type="text" name="bn_sort_en" id="bn_sort_en" class="frm_input" size="10" value="<?php echo $banner['bn_sort_en']?>"> 숫자가 높을 수록 위에 노출됩니다.
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="bn_img_en">섹션 이미지(자체 업로드)</label></th>
				<td colspan="3">
					<input type="file" name="bn_img_en" id="bn_img_en">
					<?php
					$bn_file_en = G5_DATA_PATH.'/tpl_'.$type.'/'.$banner['bn_img_en'];
					if (file_exists($bn_file_en) && $banner['bn_img_en']) {
						$bn_url_en = G5_DATA_URL.'/tpl_'.$type.'/'.$banner['bn_img_en'];
						echo '<img src="'.$bn_url_en.'" alt="" height=80> ';
						echo '<input type="checkbox" id="del_bn_url_en" name="del_bn_url_en" value="0"><input type="hidden" name="del_bn_url_en_name" id="del_bn_url_en_name" value="'.$banner['bn_img_en'].'">삭제';
					} else {
						echo '<input type="hidden" name="del_bn_url_en_name" id="del_bn_url_en_name" value="">';
					}
					?>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="bn_img_en_external">섹션 이미지(외부 이미지)</label></th>
				<td colspan="3">
					<?php
						if ($banner['bn_img_en'] && preg_match("/(http|https):/i", $banner['bn_img_en'])) {
							echo '<img src="'.$banner['bn_img_en'].'" alt="" height=80><br><br>';
							echo '<input type="text" name="bn_img_en_external" id="bn_img_en_external" class="frm_input" size="80" value="'.$banner['bn_img_en'].'">';
						} else {
							echo '<input type="text" name="bn_img_en_external" id="bn_img_en_external" class="frm_input" size="80" value="">';
						}
					?>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="bn_link_en">섹션 링크</label></th>
				<td>
					<input type="text" name="bn_link_en" id="bn_link_en" class="frm_input" size="80" value="<?php echo $banner['bn_link_en']?>">
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="bn_subject_en">섹션 제목</label></th>
				<td>
					<input type="text" name="bn_subject_en" id="bn_subject_en" class="frm_input" size="80" value='<?php echo $banner['bn_subject_en']?>'>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="bn_content_en">섹션 내용</label></th>
				<td colspan="3"><textarea name="bn_content_en" id="bn_content_en" style="height:100px;"><?php echo $banner['bn_content_en'] ?></textarea></td>
			</tr>
		</tbody>
	</table>
</div>

<div class="btn_confirm01 btn_confirm">
	<input type="submit" value="확인" class="btn_submit" accesskey='s'>
	<a href="./tpl_section_list.php?type=<?php echo $type;?>&<?php echo $qstr ?>">목록</a>
</div>
</form>

<script>
$(document).on('click change', '#del_bn_url_ko, #del_bn_url_en', function (e) {
	e.target.value = ($(this).prop('checked')) ? 1 : 0;
});
$(document).on('change', '#bn_img_ko', function (e) {
	if (e.target.value != '' && e.target.value && $('#del_bn_url_ko_name').val() != '') {
		$('#del_bn_url_ko').prop('checked', true).trigger('change');
	} else {
		$('#del_bn_url_ko').prop('checked', false).trigger('change');
	}
});
$(document).on('change', '#bn_img_en', function (e) {
	if (e.target.value != '' && e.target.value && $('#del_bn_url_en_name').val() != '') {
		$('#del_bn_url_en').prop('checked', true).trigger('change');
	} else {
		$('#del_bn_url_en').prop('checked', false).trigger('change');
	}
});
function fbanner_submit(f)
{
	if ($("#bn_state_ko > option:selected").val() == '') {
		alert("한국어 섹션 게시여부를 선택해 주세요.");
		$("#bn_state_ko").focus();
		return false;
	}
	if ($("#bn_state_en > option:selected").val() == '') {
		alert("영어 섹션 게시여부를 선택해 주세요.");
		$("#bn_state_en").focus();
		return false;
	}

	if ((($("#bn_img_ko").val() == '' && $("#del_bn_url_ko_name").val() == '') && $("#bn_img_ko_external").val() == '') && $("#bn_subject_ko").val() == '' && $("#bn_content_ko").val() == '') {
		$("#bn_img_ko").focus();
		alert("한국어 섹션 이미지/제목/내용 중 반드시 한 항목 이상 작성하셔야 합니다.");
		return false;
	}

	if ((($("#bn_img_en").val() == '' && $("#del_bn_url_en_name").val() == '') && $("#bn_img_en_external").val() == '') && $("#bn_subject_en").val() == '' && $("#bn_content_en").val() == '') {
		$("#bn_img_en").focus();
		alert("영어 섹션 이미지/제목/내용 중 반드시 한 항목 이상 작성하셔야 합니다.");
		return false;
	}

	return true;
}
</script>

<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>
