<?php
$sub_menu = "700010";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

if (!isset($type) || $type == '') {
	alert('잘못된 접근입니다!');
}

if ($_GET['del_bn_url_ko_name']) {
	$banner_file_ko = G5_DATA_PATH.'/tpl_'.$type.'/'.$_GET['del_bn_url_ko_name'];
	if (file_exists($banner_file_ko)) {
		@unlink($banner_file_ko);
	}
}

if ($_GET['del_bn_url_en_name']) {
	$banner_file_en = G5_DATA_PATH.'/tpl_'.$type.'/'.$_GET['del_bn_url_en_name'];
	if (file_exists($banner_file_en)) {
		@unlink($banner_file_en);
	}
}

if ($_GET['bn_no']) {
	sql_query("delete from mp_tpl_{$type} where bn_no = '{$_GET['bn_no']}'");
}
goto_url('./tpl_section_list.php?'.$qstr.'&amp;type='.$type, false);
?>
