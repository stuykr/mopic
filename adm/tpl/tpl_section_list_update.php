<?php
$sub_menu = "700010";
include_once('./_common.php');

check_demo();

if (!count($_POST['chk'])) {
	alert($_POST['act_button']." 하실 항목을 하나 이상 체크하세요.");
}

if (!isset($type) || $type == '') {
	alert('잘못된 접근입니다!');
}

if ($_POST['act_button'] == "선택수정") {
	auth_check($auth[$sub_menu], 'w');

	for ($i=0; $i<count($_POST['chk']); $i++) {
		// 실제 번호를 넘김
		$k = $_POST['chk'][$i];

		if ($is_admin != 'super') {
			alert('잘못된 접근입니다!');
		} else {
			$bn_sort_ko = (isset($_POST['bn_sort_ko'][$k]) && $_POST['bn_sort_ko'][$k] != '') ? $_POST['bn_sort_ko'][$k] : 0;
			$bn_sort_en = (isset($_POST['bn_sort_en'][$k]) && $_POST['bn_sort_en'][$k] != '') ? $_POST['bn_sort_en'][$k] : 0;
			
			$sql_common = "  bn_sort_ko = '{$bn_sort_ko}',
								bn_sort_en = '{$bn_sort_en}',
								bn_state_ko = '{$_POST['bn_state_ko'][$k]}',
								bn_state_en = '{$_POST['bn_state_en'][$k]}',
								bn_subject_ko = '".addslashes($_POST['bn_subject_ko'][$k])."',
								bn_subject_en = '".addslashes($_POST['bn_subject_en'][$k])."'
			";
			
			$sql = " update mp_tpl_{$type} set {$sql_common} where bn_no = '{$_POST['bn_no'][$k]}' ";
			sql_query($sql);
		}
	}

	$msg = "선택한 ".$type. " 화면 섹션을 정상적으로 수정하였습니다.";
}

alert($msg, './tpl_section_list.php?'.$qstr.'&amp;type='.$type);
?>
