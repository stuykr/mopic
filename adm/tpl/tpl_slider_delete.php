<?php
$sub_menu = "700010";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

if ($_GET['del_bn_url_ko_name']) {
	$banner_file_ko = G5_DATA_PATH.'/tpl_slider/'.$_GET['del_bn_url_ko_name'];
	if (file_exists($banner_file_ko)) {
		@unlink($banner_file_ko);
	}
}

if ($_GET['del_bn_url_en_name']) {
	$banner_file_en = G5_DATA_PATH.'/tpl_slider/'.$_GET['del_bn_url_en_name'];
	if (file_exists($banner_file_en)) {
		@unlink($banner_file_en);
	}
}

if ($_GET['bn_no']) {
	sql_query("delete from mp_tpl_slider where bn_no = '{$_GET['bn_no']}'");
}
goto_url('./tpl_slider_list.php?'.$qstr, false);
?>
