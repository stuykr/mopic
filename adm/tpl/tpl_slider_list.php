<?php
$sub_menu = "700010";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$tpl_folder = G5_DATA_PATH.'/tpl_slider';
if(!@is_dir($tpl_folder)) {
	@mkdir($tpl_folder, G5_DIR_PERMISSION);
	@chmod($tpl_folder, G5_DIR_PERMISSION);
}

$sql_common = " from mp_tpl_slider ";

$sql = " select count(*) as cnt {$sql_common} order by bn_regdt desc ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select * {$sql_common} order by bn_sort_en desc, bn_regdt desc limit {$from_record}, {$rows}";
$result = sql_query($sql);

$g5['title'] = '하단 이미지 슬라이더 관리';
include_once(G5_ADMIN_PATH.'/admin.head.php');

$colspan = 12;
?>

<div class="local_ov01 local_ov">
	등록된 이미지수 <?php echo number_format($total_count) ?>개
</div>

<div class="local_desc01 local_desc">
	<p>
		이미지를 선택(체크)하고 목록 하단 <strong>'선택수정' 버튼</strong>을 클릭하면<br>
		선택한 이미지의 <strong>정렬 순번, 게재 상태 항목이 일괄 수정</strong>됩니다.
	</p>
</div>

<?php if ($is_admin == 'super') { ?>
<div class="btn_add01 btn_add">
	<a href="./tpl_slider_form.php" id="bo_add">이미지 추가</a>
</div>
<?php } ?>

<form name="fbannerlist" id="fbannerlist" action="./tpl_slider_list_update.php" onsubmit="return fbannerlist_submit(this);" method="post">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="<?php echo $token ?>">

<div class="tbl_head01 tbl_wrap">
	<table>
	<caption><?php echo $g5['title']; ?> 목록</caption>
	<thead>
	<tr>
		<th scope="col">
			<label for="chkall" class="sound_only">수정할 섹션 전체 선택</label>
			<input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
		</th>
		<th scope="col">정렬 순번</th>
		<th scope="col">이미지</th>
		<th scope="col">게재상태</th>
		<th scope="col">등록일</th>
		<th scope="col">관리</th>
	</tr>
	</thead>
	<tbody>
	<?php
	for ($i=0; $row=sql_fetch_array($result); $i++) {
		unset($bn_img_ko);
		unset($bn_img_en);
		$del_img_str = '';
		$bg = 'bg'.($i%2);

		// 외부 이미지 경로가 있을 경우 자체 업로드보다 높은 우선순위 적용
		if ($row['bn_img_ko'] && preg_match("/(http|https):/i", $row['bn_img_ko'])) {
			$bn_img_ko = '<img src="'.$row['bn_img_ko'].'" alt="" height=80> ';
		} else {
			$bn_file_ko = G5_DATA_PATH.'/tpl_slider/'.$row['bn_img_ko'];
			if (file_exists($bn_file_ko) && $row['bn_img_ko']) {
				$del_img_str .= '&amp;del_bn_url_ko_name'.$row['bn_img_ko'];
				$bn_url_ko = G5_DATA_URL.'/tpl_slider/'.$row['bn_img_ko'];
				$bn_img_ko = '<img src="'.$bn_url_ko.'" alt="" height=80> ';
			}
		}
		if ($row['bn_img_en'] && preg_match("/(http|https):/i", $row['bn_img_en'])) {
			$bn_img_en = '<img src="'.$row['bn_img_en'].'" alt="" height=80> ';
		} else {
			$bn_file_en = G5_DATA_PATH.'/tpl_slider/'.$row['bn_img_en'];
			if (file_exists($bn_file_en) && $row['bn_img_en']) {
				$del_img_str .= '&amp;del_bn_url_en_name'.$row['bn_img_en'];
				$bn_url_en = G5_DATA_URL.'/tpl_slider/'.$row['bn_img_en'];
				$bn_img_en = '<img src="'.$bn_url_en.'" alt="" height=80> ';
			}
		}
		switch($row['bn_state_ko']) {
			case '1': $bn_state_ko = "O"; break;
			case '2': $bn_state_ko = "X"; break;
		}
		switch($row['bn_state_en']) {
			case '1': $bn_state_en = "O"; break;
			case '2': $bn_state_en = "X"; break;
		}

		$one_update = '<a href="./tpl_slider_form.php?w=u&amp;bn_no='.$row['bn_no'].'&amp;'.$qstr.'">수정</a>';
		$one_del = '<a href="./tpl_slider_delete.php?bn_no='.$row['bn_no'].'&amp;'.$qstr.$del_img_str.'" onclick="if(!confirm(\'이 이미지를 정말로 삭제하시겠습니까?\')) return false;">삭제</a>';
	?>
	<tr class="<?php echo $bg; ?>">
		<td class="td_chk">
			<label for="chk_<?php echo $i; ?>" class="sound_only"><?php echo $row['bn_no'] ?></label>
			<input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>">
			<input type="hidden" name="bn_no[<?php echo $i ?>]" value="<?php echo $row['bn_no'] ?>">
		</td>
		<td align="center">
			<p><label for="bn_sort_ko_<?php echo $i; ?>">한: </label><input type="text" name="bn_sort_ko[<?php echo $i ?>]" value="<?php echo $row['bn_sort_ko'] ?>" id="bn_sort_ko_<?php echo $i ?>" class="frm_input" size="5"></p>
			<p><label for="bn_sort_en_<?php echo $i; ?>">영: </label><input type="text" name="bn_sort_en[<?php echo $i ?>]" value="<?php echo $row['bn_sort_en'] ?>" id="bn_sort_en_<?php echo $i ?>" class="frm_input" size="5"></p>
		</td>
		<td align="center">
			<p>한: <?php echo $bn_img_ko;?></p>
			<p>영: <?php echo $bn_img_en;?></p>
		</td>
		<td align="center">
			<p><label for="bn_state_ko_<?php echo $i; ?>">한: </label>
			<select name="bn_state_ko[<?php echo $i ?>]" id="bn_state_ko_<?php echo $i ?>" required class="frm_input">
				<option value="1" <?php if($row['bn_state_ko'] == '1') echo "selected";?>>O 보이기</option>
				<option value="2" <?php if($row['bn_state_ko'] == '2') echo "selected";?>>X 숨기기</option>
			</select></p>
			<p><label for="bn_state_en_<?php echo $i; ?>">영: </label>
			<select name="bn_state_en[<?php echo $i ?>]" id="bn_state_en_<?php echo $i ?>" required class="frm_input">
				<option value="1" <?php if($row['bn_state_en'] == '1') echo "selected";?>>O 보이기</option>
				<option value="2" <?php if($row['bn_state_en'] == '2') echo "selected";?>>X 숨기기</option>
			</select></p>
		</td>
		<td align="center">
			<?php echo $row['bn_regdt']?>
		</td>
		<td class="td_mngsmall">
			<?php echo $one_update ?>&nbsp;&nbsp;<?php echo $one_del ?>
		</td>
	</tr>
	<?php
	}
	if ($i == 0)
		echo '<tr><td colspan="'.$colspan.'" class="empty_table">등록된 이미지가 없습니다.</td></tr>';
	?>
	</tbody>
	</table>
</div>
<div class="btn_list01 btn_list">
	<input type="submit" name="act_button" value="선택수정" onclick="document.pressed=this.value">
</div>
</form>

<?php echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, $_SERVER['PHP_SELF'].'?'.$qstr.'&amp;page='); ?>

<script>
function fbannerlist_submit(f)
{
	if (!is_checked('chk[]')) {
		alert(document.pressed + ' 하실 이미지를 하나 이상 선택하세요.');
		return false;
	}
	return true;
}
</script>

<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>
