<?php 
include_once('lan.php');
include_once(G5_LIB_PATH.'/thumbnail.lib.php');

$nation = "1";
if($lng=="ko") $nation = "1";
if($lng=="en") $nation = "2";
if($lng=="ch") $nation = "3";
if($lng=="jp") $nation = "4";


$qstr .= "&lng=".$lng;
?>

<!doctype html>
<html lang="<?=$lngType;?>">
<head>
<?php include_once(MOPIC_LAYOUT_URL.'/mopicS.php'); ?>
</head>
<body>
<div id="wrap">
	<header class="header">
		<?php include_once(MOPIC_LAYOUT_URL.'/mopicH.php'); ?>
	</header>
	<main class="contents container">
	<div id="product">
		<div>
			<ul id="productCate">
				<li><a href="?sca=1&lng=<?=$lng?>" <?if($sca=="1") echo 'class="on"'; ?>>Galaxy</a></li>
				<li><a href="?sca=2&lng=<?=$lng?>" <?if($sca=="2") echo 'class="on"'; ?>>iPhone</a></li>
				<li><a href="?sca=3&lng=<?=$lng?>" <?if($sca=="3") echo 'class="on"'; ?>>Other</a></li>
			</ul>
			<ul id="productList">
				<?php
				$sql_common = " from MP_ITEM ";

				if($sca){
					$sql_common .= " where 	( it_cate='{$sca}' or it_cate='4' ) ";
				}

				$sql = " select count(*) as cnt " . $sql_common;
				$row = sql_fetch($sql);
				$total_count = $row['cnt'];

				$rows = 9;
				$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
				if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
				$from_record = ($page - 1) * $rows; // 시작 열을 구함

				$sql = "select * $sql_common order by it_id desc limit {$from_record}, {$rows} ";
				$result = sql_query($sql);
				for ($i=0; $row=sql_fetch_array($result); $i++) {
					$thumb = get_app_thumbnail("MP_ITEM", $row['it_id'], 233, 330);
					$pt_img = $thumb['src']; 

					$thumb = get_app_thumbnail("MP_ITEM", $row['it_id'], 783, '', $nation);
					$ex_img = $thumb['src']; 
				?>
				<li>
					<?if($pt_img){?><img src="<?=$pt_img?>" <?if($ex_img){?>data-cont="<?=$ex_img?>"<?}?> /><?}?>
					<strong><?=$row['it_name']?></strong>
					<span><?=$row['it_model']?></span>
					<?if($row['it_url']){?>
						<a href="<?=$row['it_url']?>" class="product-btn-buy" target="_blank"><?php if ($lng == 'ko') { echo '구매하기'; } else { echo 'SHOP NOW'; } ?></a>
					<?}else{?>
						<a href="#applyPop" class="btn_popup product-btn-buy">CONTACT US</a>
					<?php } ?>
				</li>
				<?php } ?>

			</ul>
			<div id="pagenation">
				<?php echo get_paging_front(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, '?'.$qstr.'&amp;page='); ?>
			</div>
		</div>
	</div>
	</main>
	<div id="applyPop">
		<div>
			<img src="<?php echo G5_URL; ?>/img/pop/img_title.jpg" class="imgPc" />
			<img src="<?php echo G5_URL; ?>/img/pop/img_m_title.jpg" class="imgMb" />
			<div>
				<div>
					<form method="post" id="mailForm" action="<?php echo G5_URL; ?>/pages/mailSend.php">
						<input type="hidden" id="mail_lngType" name="mail_lngType" value="<?=$lngType;?>" />
						<input type="text" id="mailName" name="mailName" placeholder="Name" />
						<input type="text" id="mailEmail" name="mailEmail" placeholder="E-mail Address" />
						<textarea placeholder="Message" id="mailCont" name="mailCont"></textarea>
						<a href="javascript:;" id="mailSend"><img src="<?php echo G5_URL; ?>/img/pop/btn_submit.jpg" /></a>
					</form>
				</div>
				<img src="<?php echo G5_URL; ?>/img/pop/img_info.jpg" />
			</div>
			<a href="#applyPop" class="applyClose"><img src="<?php echo G5_URL; ?>/img/pop/btn_close.jpg" /></a>
		</div>
	</div>
	<footer class="footer">
		<?php include_once(MOPIC_LAYOUT_URL.'/mopicF.php'); ?>
	</footer>
</div>

<script type="text/javascript">
$(function(){
	$("#productList > li > img").click(function(){
		var contSrc = $(this).data("cont");
		if(contSrc){
			var productPop = window.open("<?php echo G5_URL; ?>/pages/layout/pop.php?contSrc="+contSrc, 'pop', 'scrollbars=yes,resizable=no,status=no,location=no,toolbar=no,menubar=no,top=200,left=200,width=800,height=600');
		}
	});
	
});
</script>

<script type="text/javascript" src="http://wcs.naver.net/wcslog.js"></script>
<script type="text/javascript">
if(!wcs_add) var wcs_add = {};
wcs_add["wa"] = "b288e8e3e4c0c";
wcs_do();
</script>

</body>
</html>
<?php echo html_end(); ?>