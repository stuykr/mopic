<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_LIB_PATH.'/thumbnail.lib.php');

$begin_time = get_microtime();
$g5['lo_location'] = addslashes($g5['title']);
if (!$g5['lo_location'])
    $g5['lo_location'] = addslashes(clean_xss_tags($_SERVER['REQUEST_URI']));
$g5['lo_url'] = addslashes(clean_xss_tags($_SERVER['REQUEST_URI']));
if (strstr($g5['lo_url'], '/'.G5_ADMIN_DIR.'/') || $is_admin == 'super') $g5['lo_url'] = '';
?>
<!doctype html>
<html lang="<?=$lngType;?>">
<head>
<?php include_once(MOPIC_LAYOUT_URL.'/mopicS.php'); ?>
</head>
<body>
<?php include_once(G5_BBS_PATH.'/newwin.inc.php'); // 팝업레이어 ?>
<div id="wrap">
	<header class="header">
		<?php include_once(MOPIC_LAYOUT_URL.'/mopicH.php'); ?>
	</header>
	<main class="contents container">
		<div id="mopicBoard">