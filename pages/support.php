<?php 
include_once('lan.php');
define('_INDEX_', true);
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

$row_url = sql_fetch("select * from MP_BANNER where bn_id='21'");
?>

<!doctype html>
<html lang="<?=$lngType;?>">
<head>
<?php include_once(MOPIC_LAYOUT_URL.'/mopicS.php'); ?>
</head>
<body>
<div id="wrap">
	<header class="header">
		<?php include_once(MOPIC_LAYOUT_URL.'/mopicH.php'); ?>
	</header>
	<main class="contents container">
		<div id="support01">
			<div>
			
	<!-- 			<img src="<?php echo G5_URL; ?>/img/support/video_temp.jpg" /> -->
				
				<div id="videoWrap">
					<?php
					if($lngType == "ko"){
					?>
						<iframe id="youtubeVideo" src="https://www.youtube.com/embed/_sxylThlzus" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					<?php
					} else if($lngType == "en"){
					?>
						<iframe id="youtubeVideo" src="<?=$row_url['bn_url']?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					<?php
					} else if($lngType == "jp"){
					?>
						<iframe id="youtubeVideo" src="https://www.youtube.com/embed/bk6oZAS5i4c" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					<?php
					} else if($lngType == "ch"){
					?>
				<video src="<?php echo G5_URL; ?>/mplayer3d/tutorial/Mplayer3D_CN_Final.mp4" width="100%" controls></video>
					<?php
					}
					?>
	<!--				<iframe id="youtubeVideo" src="<?=$row_url['bn_url']?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
	<!-- 				<iframe id="youtubeVideo" src="https://www.youtube.com/embed/Gelte8XYVgM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
				</div>
				<br><br>
				<ul id="downList">
					<li><span><B>Mplayer3D</B></span></li>
				</ul>
				<ul id="downList">
					<li>
						<span>Download</span>
						<ul>
							<li><a href="https://itunes.apple.com/app/mplayer3d/id1191424861" target="_blank"><img src="<?php echo G5_URL; ?>/img/support/btn_down03.jpg" /></a></li>
							<li><a href="<?php echo G5_URL; ?>/mplayer3d/download/android_download.html" target="_blank"><img src="<?php echo G5_URL; ?>/img/support/btn_down04.jpg" /></a></li>
						</ul>
					</li>
					<li>
						<span>User Manual</span>
						<ul>
							<li><a href="<?php echo G5_URL; ?>/mplayer3d/manual/iOS_manual_download.html" target="_blank"><img src="<?php echo G5_URL; ?>/img/support/btn_down01.jpg" /></a></li>
							<li><a href="<?php echo G5_URL; ?>/mplayer3d/manual/Android_manual_download.html" target="_blank"><img src="<?php echo G5_URL; ?>/img/support/btn_down02.jpg" /></a></li>
						</ul>
					</li>
				</ul>
				<br><br><br>
				<ul id="downList">
					<li><span><B>Mcamera3D</B></span></li>
				</ul>
				<ul id="downList">
					<li>
						<span>Download</span>
						<ul>
							<li><a href="https://itunes.apple.com/app/mplayer3d/id1441718711" target="_blank"><img src="<?php echo G5_URL; ?>/img/support/btn_down03.jpg" /></a></li>
						</ul>
					</li>
					<li>
						<span>User Manual</span>
						<ul>
							<li><a href="<?php echo G5_URL; ?>/mcamera3d/manual/iOS_manual_download.html" target="_blank"><img src="<?php echo G5_URL; ?>/img/support/btn_down01.jpg" /></a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		</main>
	<footer class="footer">
		<?php include_once(MOPIC_LAYOUT_URL.'/mopicF.php'); ?>
	</footer>
</div>

<script type="text/javascript">
$(function(){
	$("#videoWrap").css("height",$("#videoWrap").outerWidth() * 0.55987394957983193277310924369748);
	$("#youtubeVideo").attr("width",$("#videoWrap").outerWidth());
	$("#youtubeVideo").attr("height",$("#videoWrap").outerWidth() * 0.55987394957983193277310924369748);

	$(window).resize(function(){
		$("#videoWrap").css("height",$("#videoWrap").outerWidth() * 0.55987394957983193277310924369748);
		$("#youtubeVideo").attr("width",$("#videoWrap").outerWidth());
		$("#youtubeVideo").attr("height",$("#videoWrap").outerWidth() * 0.55987394957983193277310924369748);
	});
});
</script>

<script type="text/javascript" src="http://wcs.naver.net/wcslog.js"></script>
<script type="text/javascript">
if(!wcs_add) var wcs_add = {};
wcs_add["wa"] = "b288e8e3e4c0c";
wcs_do();
</script>
</body>
</html>
<?php echo html_end(); ?>