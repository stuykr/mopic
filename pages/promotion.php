<?php include_once('./_common.php'); ?>
<?php include_once(MOPIC_PAGES_URL.'/section/head.php'); ?>
<main class="contents">
	<?php echo mp_section('promotion', $lngType); ?>

	<?php echo mp_slider($lngType); ?>

	<?php include_once(MOPIC_LAYOUT_URL.'/mopicContactUs.php'); ?>
</main>
<?php include_once(MOPIC_PAGES_URL.'/section/tail.php'); ?>