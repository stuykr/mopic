<div class="header-toparea">
	<div class="container">
		<div class="header-topinfo">
		</div>
		<div class="header-language">
			<ul>
				<li><a href="<?php echo MOPIC_SITE_URL; ?>?lng=ko" class="<?=$lngSlc01;?>">한국어</a></li>
				<li><a href="<?php echo MOPIC_SITE_URL; ?>?lng=en" class="<?=$lngSlc02;?>">English</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="header-contents container">
	<div class="header-logo">
		<a href="<?php echo MOPIC_SITE_URL; ?>?lng=<?=$lngType;?>"><img src="<?php echo G5_URL; ?>/img/logo_renewal.png" /></a>
	</div>
	<nav class="gnb">
		<?php include(MOPIC_LAYOUT_URL.'/mopicNav.php'); ?>
	</nav>
	<div class="gnb-mobile-btn">
		<a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
	</div>
	<nav class="gnb-mobile">
		<a href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
		<?php include(MOPIC_LAYOUT_URL.'/mopicNav.php'); ?>
	</nav>
	<div class="gnb-mobile-overlay"></div>
</div>