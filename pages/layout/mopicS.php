<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<link rel="canonical" href="http://mopic3d.com">
<link rel="icon" href="<?php echo G5_URL; ?>/mopic.ico" type="image/vnd.microsoft.icon">
<meta name="description" content="By minimizing the difference between reality and display through 3D tech, MOPIC creates greater value.">
<meta name="title" content="MOPIC">
<meta property="og:type" content="website">
<meta property="og:title" content="MOPIC">
<meta property="og:description" content="By minimizing the difference between reality and display through 3D tech, MOPIC creates greater value.">
<meta property="og:image" content="http://mopic3d.com/img/img_logo2.jpg">
<meta property="og:url" content="http://mopic3d.com/">
<title>MOPIC</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/earlyaccess/nanumgothic.css">
<?php add_stylesheet('<link rel="stylesheet" href="'.G5_URL.'/css/reset.css" type="text/css">', 0); ?>
<?php add_stylesheet('<link rel="stylesheet" href="'.G5_URL.'/css/layout.css" type="text/css">', 0); ?>
<?php add_stylesheet('<link rel="stylesheet" href="'.G5_URL.'/css/res.css" type="text/css" media="screen and (max-width: 720px)">', 0); ?>
<script src="<?php echo G5_JS_URL ?>/jquery-1.8.3.min.js"></script>
<script src="<?php echo G5_URL; ?>/css/jquery.bxslider.min.js"></script>
<!--[if lte IE 8]>
<script src="<?php echo G5_JS_URL ?>/html5.js"></script>
<![endif]-->
<script>
// 자바스크립트에서 사용하는 전역변수 선언
var g5_url       = "<?php echo G5_URL ?>";
var g5_bbs_url   = "<?php echo G5_BBS_URL ?>";
var g5_is_member = "<?php echo isset($is_member)?$is_member:''; ?>";
var g5_is_admin  = "<?php echo isset($is_admin)?$is_admin:''; ?>";
var g5_is_mobile = "<?php echo G5_IS_MOBILE ?>";
var g5_bo_table  = "<?php echo isset($bo_table)?$bo_table:''; ?>";
var g5_sca       = "<?php echo isset($sca)?$sca:''; ?>";
var g5_editor    = "<?php echo ($config['cf_editor'] && $board['bo_use_dhtml_editor'])?$config['cf_editor']:''; ?>";
var g5_cookie_domain = "<?php echo G5_COOKIE_DOMAIN ?>";
<?php if(defined('G5_IS_ADMIN')) { ?>
var g5_admin_url = "<?php echo G5_ADMIN_URL; ?>";
<?php } ?>
</script>
<script src="<?php echo G5_JS_URL ?>/jquery.menu.js?ver=<?php echo G5_JS_VER; ?>"></script>
<script src="<?php echo G5_JS_URL ?>/common.js?ver=<?php echo G5_JS_VER; ?>"></script>
<script src="<?php echo G5_JS_URL ?>/wrest.js?ver=<?php echo G5_JS_VER; ?>"></script>