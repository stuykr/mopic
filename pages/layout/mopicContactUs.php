<div id="applyPop" class="layerPopup">
	<div class="layerBody">
		<button type="button" class="layer-btn-close">Close</button>
		<div class="pop-title">
			<h3>Contact Us</h3>
		</div>
		<div class="pop-form">
			<form method="post" id="mailForm" action="<?php echo G5_URL; ?>/pages/mailSend.php">
				<input type="hidden" id="mail_lngType" name="mail_lngType" value="<?=$lngType;?>">
				<input type="text" id="mailName" name="mailName" class="pop-form-ipt" placeholder="Name" required>
				<input type="email" id="mailEmail" name="mailEmail" class="pop-form-ipt" placeholder="E-mail Address" required>
				<textarea placeholder="Message" id="mailCont" class="pop-form-ipt" name="mailCont" required></textarea>
				<button type="submit" id="mailSend" class="layer-btn-confirm">Send</button>
			</form>
		</div>
	</div>
</div>