<div class="footer-contents container">
	<img class="footer-logo" src="<?php echo G5_URL; ?>/img/logo_renewal_txt.png" />
	<div class="footer-nav">
		<ul>
			<?php if ($lng == 'ko') { ?>
				<li><a href="<?php echo G5_BBS_URL; ?>/board.php?bo_table=newsroom_ko&lng=ko">NEWSROOM</a></li>
				<li><a href="<?php echo G5_BBS_URL; ?>/board.php?bo_table=support_ko&lng=ko">SUPPORT</a></li>
			<?php } else { ?>
				<li><a href="<?php echo G5_BBS_URL; ?>/board.php?bo_table=newsroom&lng=en">NEWSROOM</a></li>
				<li><a href="<?php echo G5_BBS_URL; ?>/board.php?bo_table=support&lng=en">SUPPORT</a></li>
			<?php } ?>
			<li><a href="<?php echo MOPIC_SITE_URL; ?>/business?lng=<?=$lngType;?>">BUSINESS</a></li>
			<?php if ($lng == 'ko') { ?>
			<li><a href="<?php echo MOPIC_SITE_URL; ?>/privacy?lng=<?=$lngType;?>">PRIVACY</a></li>
			<?php } ?>
			<li><a href="https://drive.google.com/drive/u/1/folders/1rMxG47vQ27zQ2vuvKxioiidZ2LX5lWI5" target="_blank">PRESS KIT</a></li>
		</ul>
	</div>
	<div class="footer-info">
		<ul>
			<?php if ($lng == 'ko') { ?>
			<li class="location lng-kr">(14058)경기도 안양시 동안구 벌말로 66 평촌하이필드지식산업센터 B동 1105호</li>
			<?php } else { ?>
			<li class="location">#1105, B-Building, 66, Beolmal-ro, Dongan-gu, Anyang-si Gyeonggi-do, Republic of Korea. 14058.</li>
			<?php } ?>
			<li><a href="mailto:info@mopic.co.kr"><i class="fa fa-envelope" aria-hidden="true"></i>info@mopic.co.kr</a></li>
			<li><a href="tel:+82 31-8084-9866"><i class="fa fa-phone-square" aria-hidden="true"></i>+82-31-8084-9866</a></li>
		</ul>
		<p class="copyright">Copyright &copy; MOPIC Co., Ltd. All Rights Reserved</p>
	</div>
</div>

<script type="text/javascript">
$(function() {
	var $window = $(window);
	var MOPIC_UI = {};
	MOPIC_UI.layerControl = {
		openLayer: function (target) {
			var isVisible = $(target).is(':visible');
			if (!isVisible) {
				var $target = $(target);
				if (!$('.overlay').length) {
					$('body').before('<div class="overlay"></div>');
				}
				$('.layerPopup:visible').addClass('inactive');
				$('body').addClass('locked-scroll');
				$('.overlay').fadeIn(150);
				this.setLayerSize(target, function () {
					$target.css({ 'z-index': 300 + $('.layerPopup:visible').length }).fadeIn(150);
				});
			}
		},
		closeLayer: function (target) {
			var isVisible = $(target).is(':visible');
			if (isVisible) {
				var $target = $(target);
				if ($('.layerPopup:visible').length > 1) {
					this.getHighestLayer(target).removeClass('inactive');
				} else {
					$('.overlay').fadeOut(150);
					$('body').removeClass('locked-scroll');
				}
				$target.fadeOut(150, function () {
					$target.removeClass('inactive').removeAttr('style');
				});
				if ($target.find('iframe').length) {
					$target.find('iframe').remove();
				}
			}
		},
		getHighestLayer: function (target) {
			var highestLayer = '',
				highestIdx = 0;
			$('.layerPopup:visible').not(target).each(function () {
				var zIdx = parseInt($(this).css('z-index'), 10);
				if (zIdx > highestIdx) {
					highestLayer = $(this);
					highestIdx = zIdx;
				}
			});
			return highestLayer;
		},
		setLayerSize: function (target, callBack) {
			if ($(target).length) {
				var $target = $(target),
					timeLimit = 130;
				setTimeout(function () {
					var targetHeight = $target.find('.layerBody').height();
						layerTopMargin = 0,
						min = 100,
						max = $window.height;

					layerTopMargin = ($window.height() < targetHeight + min) ? min : ($window.height() - targetHeight) / 2;

					if (callBack && $.isFunction(callBack)) {
						$target.find('.layerBody').css({ top: layerTopMargin });
						callBack();
					} else {
						$target.find('.layerBody').stop().animate({ top: layerTopMargin }, 250);
					}
				}, timeLimit);
			}
		}
	};

	$('.gnb-mobile-btn > a, .gnb-mobile > a').on('click', function(e) {
		e.preventDefault();
		$('.gnb-mobile, .gnb-mobile-overlay').toggleClass('opened');
	});
	$('.gnb-mobile-overlay').on('click', function() {
		$('.gnb-mobile, .gnb-mobile-overlay').removeClass('opened');
	});

	$('.btn_popup').on('click', function (e) {
		e.preventDefault();
		var target = $(this).attr('href');
		MOPIC_UI.layerControl.openLayer(target);
	});

	$('.layerPopup .layer-btn-close, .layerPopup .layer-btn-cancel, .overlay').on('click', function (e) {
		e.preventDefault();
		var target = '#' + $(this).parents('.layerPopup').attr('id');
		MOPIC_UI.layerControl.closeLayer(target);
	});
	$('.layerBody').on('click mousemove touchstart touchend touchmove', function (e) {
		e.stopPropagation();
	});
	$('.layerPopup').on('click', function () {
		if ($(this).is(':visible')) {
			var target = '#' + $(this).attr('id');
			MOPIC_UI.layerControl.closeLayer(target);
		}
	});

	$('#mailSend').on('click', function() {
		$('#mailForm').submit();
	});

	var agent = navigator.userAgent.toLowerCase();
	if ((navigator.appName == 'Netscape' && agent.indexOf('trident') != -1) || (agent.indexOf('msie') != -1)) {
		$('.slider').bxSlider({
			controls: false,
			touchEnabled: true
		});
		// ie일 경우
	} else {
		// ie가 아닐 경우
		$('.slider').bxSlider({
			controls: false
		});
	}

	$window.on('resize', function () {
		MOPIC_UI.layerControl.setLayerSize($('.layerPopup:visible'));
	});
});
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117603512-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-117603512-1');
</script>