<?php
if (strpos(G5_URL.$_SERVER['REQUEST_URI'], 'renewal') === false) {
	define('MOPIC_SITE_URL',        G5_URL);
	define('MOPIC_URL',             $_SERVER['DOCUMENT_ROOT']);
	define('MOPIC_PAGES_URL',      $_SERVER['DOCUMENT_ROOT'].'/pages');
	define('MOPIC_LAYOUT_URL',      $_SERVER['DOCUMENT_ROOT'].'/pages/layout');
} else {
	define('MOPIC_SITE_URL',        G5_URL.'/renewal');
	define('MOPIC_URL',             $_SERVER['DOCUMENT_ROOT'].'/renewal');
	define('MOPIC_LAYOUT_URL',      $_SERVER['DOCUMENT_ROOT'].'/renewal/layout');
}
// CSS, JS 캐시 대응 Ver 설정
$fileDate = date("YmdHis", G5_SERVER_TIME);
define('G5_CSS_VER', $fileDate);
define('G5_JS_VER',  $fileDate);

function mp_section($type, $lng) {
	$link_path = G5_DATA_URL.'/tpl_'.$type;

	$sql = "select * from mp_tpl_{$type} where bn_state_{$lng} = '1' order by bn_sort_{$lng} desc, bn_regdt desc";
	$result = sql_query($sql, false);
	$txt_more = ($lng == 'ko') ? '더 보기' : 'Learn More';

	for ($i = 0; $row = sql_fetch_array($result); $i++) {
		$banner[$i] = $row;
		if (is_array($banner[$i]) && !empty($banner[$i])) {
			if ($banner[$i]['bn_img_'.$lng] != '') {
				$img = $banner[$i]['bn_img_'.$lng];
				$banner[$i]['image'] = ($img && preg_match("/(http|https):/i", $img)) ? $img : $link_path.'/'.$img;
				$banner[$i]['tag_img'] = '<img src="'.$banner[$i]['image'].'">';
			} else {
				$banner[$i]['tag_img'] = '';
			}
			$banner[$i]['bn_subject_'.$lng] = stripslashes($banner[$i]['bn_subject_'.$lng]);
			$banner[$i]['bn_content_'.$lng] = stripslashes($banner[$i]['bn_content_'.$lng]);
			if ($banner[$i]['bn_content_'.$lng] && strpos($banner[$i]['bn_content_'.$lng], "[=더보기]") !== false) {
				$banner[$i]['tag_more_'.$lng] = '<div class="post-item-btn"><a href="'.$banner[$i]['bn_link_'.$lng].'" target="'.$banner[$i]['bn_target'].'">'.$txt_more.'<i class="fa fa-angle-right" aria-hidden="true"></i></a></div>';
				$banner[$i]['bn_content_'.$lng] = str_replace("[=더보기]", '', $banner[$i]['bn_content_'.$lng]);
			} else {
				$banner[$i]['tag_more_'.$lng] = '';
			}
		}
	}

	if (is_array($banner) && !empty($banner)) {
		foreach ($banner as $item) {
			$result = '';
			$result .= '<div class="post-item lng-'.$lng.'">';

			$str_tit = ($item['bn_subject_'.$lng] != '') ? '<h2 class="tit">'.$item['bn_subject_'.$lng].'</h2>' : '';
			$str_desc = ($item['bn_content_'.$lng] != '') ? '<p class="desc">'.$item['bn_content_'.$lng].'</p>'.$item['tag_more_'.$lng] : '';

			if ($item['tag_img'] != '') {
				$result .= '<div class="post-item-img">';
				$str_img = ($item['bn_link_'.$lng] != '') ? '<a id="banner_'.$item['bn_no'].'" href="'.$item['bn_link_'.$lng].'" target="'.$item['bn_target'].'">'.$item['tag_img'].'</a>' : $item['tag_img'];
				$result .= $str_img;
				$result .= '</div>';
			}
			if ($str_tit != '' || $str_desc != '') {
				$result .= '<div class="post-item-txt">';
				$result .= $str_tit;
				$result .= $str_desc;
				$result .= '</div>';
			}
			$result .= '</div>';

			echo $result;
		}
	}
}

function mp_slider($lng) {
	$link_path = G5_DATA_URL.'/tpl_slider';

	$sql = "select * from mp_tpl_slider where bn_state_{$lng} = '1' order by bn_sort_{$lng} desc, bn_regdt desc";
	$result = sql_query($sql, false);

	for ($i = 0; $row = sql_fetch_array($result); $i++) {
		$banner[$i] = $row;
		if (is_array($banner[$i]) && !empty($banner[$i])) {
			if ($banner[$i]['bn_img_'.$lng] != '') {
				$img = $banner[$i]['bn_img_'.$lng];
				$banner[$i]['image'] = ($img && preg_match("/(http|https):/i", $img)) ? $img : $link_path.'/'.$img;
				$banner[$i]['tag_img'] = '<img src="'.$banner[$i]['image'].'">';
			} else {
				$banner[$i]['tag_img'] = '';
			}
		}
	}

	if (is_array($banner) && !empty($banner)) {
		$result = '';
		$result .= '<div class="slider-wrap lng-'.$lng.'"><ul class="slider">';
		foreach ($banner as $item) {
			if ($item['tag_img'] != '') {
				$result .= '<li class="slider-item" style="background-image:url(\''.$item['image'].'\')";">';
				$str_img = ($item['bn_link_'.$lng] != '') ? '<a id="banner_'.$item['bn_no'].'" href="'.$item['bn_link_'.$lng].'" target="'.$item['bn_target'].'">'.$item['tag_img'].'</a>' : $item['tag_img'];
				$result .= $str_img;
				$result .= '</li>';
			}
		}
		$result .= '</ul><a href="#applyPop" class="btn_popup" class="slider-btn">Contact Us</a></div>';
		echo $result;
	}
}
?>